# Project: codfis

CPP  = g++
OBJ  = src/main.o src/calccf.o src/dlg_db_ch.o src/struty.o src/dlg_info.o
LINKOBJ  = src/main.o src/calccf.o src/dlg_db_ch.o src/struty.o src/dlg_info.o
LIBS =  -L/usr/X11R6/lib -lXext -lX11 -lm -lfltk -lsqlite3 -lXpm
CXXINCS =  -I"include"
BIN  = codfis
DB = codfis.db3
DBSQL = src/codfis.sql
CXXFLAGS = -O2 -Wall -static
RM = rm -f

all: $(BIN) $(DB)

$(BIN): $(OBJ)
	$(CPP) $(LINKOBJ) -o $(BIN) $(LIBS)

src/main.o: src/main.cpp
	$(CPP) -c src/main.cpp -o src/main.o $(CXXFLAGS) $(CXXINCS)

src/calccf.o: src/calccf.cpp
	$(CPP) -c src/calccf.cpp -o src/calccf.o $(CXXFLAGS) $(CXXINCS)

src/dlg_db_ch.o: src/dlg_db_ch.cpp
	$(CPP) -c src/dlg_db_ch.cpp -o src/dlg_db_ch.o $(CXXFLAGS) $(CXXINCS)

src/struty.o: src/struty.cpp
	$(CPP) -c src/struty.cpp -o src/struty.o $(CXXFLAGS) $(CXXINCS)

src/dlg_info.o: src/dlg_info.cpp
	$(CPP) -c src/dlg_info.cpp -o src/dlg_info.o $(CXXFLAGS) $(CXXINCS)

$(DB): $(DBSQL)
	sqlite3 -init $(DBSQL) $(DB) .quit
#	sqlite3 $(DB) < $(DBSQL)

clean:
	${RM} $(OBJ) $(BIN) $(DB)
