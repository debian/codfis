/**
   @license GNU GPLv2
   PROJECT "codfis"
   Copyright � 2005,2006 Danilo Cicerone

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

/**
   @file struty.h
   @author Danilo Cicerone info@digitazero.org
   @date 2005-06-12
   @version 0.08
*/

#ifndef STRUTY_H
#define STRUTY_H

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

class StrUty
{
   public:
      void
      split(const string& input, const string& delimiter, vector<string>& results);

      string
      trim(const string& input);

      int
      toint(const string& input);

      double
      todouble(const string& input);

      string
      int2string(const int& input);

      string
      lcase(const string& input);

      string
      ucase(const string& input);
};

#endif
