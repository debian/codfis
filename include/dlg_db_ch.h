/**
   @license GNU GPLv2
   PROJECT "codfis"
   Copyright � 2005,2006 Danilo Cicerone

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

/**
   @file dlg_db_ch.h
   @author Danilo Cicerone info@digitazero.org
   @date 2006-05-11
   @version 0.4.7
*/

#ifndef SELEDB_H
#define SELEDB_H

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Button.H>
#include <FL/fl_ask.H>

//#include <iostream>
#include <string>
#include <stdlib.h>
#include <sqlite3.h>

using namespace std;

class WinSelDB
{
   public:
      WinSelDB();
      ~WinSelDB();
      void show();
      void hide();
      int visible();
      int standby(string &strcod, string &strcom);

   private:
      Fl_Window *dlg_sele;
      Fl_Box *fra_sel;
      Fl_Hold_Browser *lst_db;
      Fl_Input *txt_sch1;
      Fl_Input *txt_sch2;
      Fl_Return_Button *btn_ok;
      Fl_Button *btn_annulla;
      int clicked;
      sqlite3 *db;

      void 
      close_db();

      static void
      cb_lst_db(Fl_Hold_Browser*, void*);

      static void
      cb_txt_sch1(Fl_Input*, void*);

      static void
      cb_txt_sch2(Fl_Input*, void*);

      static void
      cb_btn_ok(Fl_Return_Button*, void*);

      static void
      cb_btn_annulla(Fl_Button*, void*);

      inline void
      ex_ok();

      inline void
      ex_query();

      inline void
      ex_annulla();
};

#endif
