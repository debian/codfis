/**
   @license GNU GPLv2
   PROJECT "codfis"
   Copyright � 2005,2006 Danilo Cicerone

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

/**
   @file calccf.h
   @author Danilo Cicerone info@digitazero.org
   @date 2006-05-11
   @version 0.4.7
*/

#ifndef CALCCF_H
#define CALCCF_H

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/fl_ask.H>

#if defined(WIN32)
   // Nothing to do here (Dev-C++ solve this problem)
#elif defined(__APPLE__)
   // Nothing to do here
#else
   #include <FL/x.H>
   #include <X11/xpm.h>
   #include "codfis.xpm"
#endif

#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>

#include "dlg_db_ch.h"
#include "dlg_info.h"
#include "struty.h"

using namespace std;

class CFWin {
   public:
      CFWin();
      ~CFWin();
      void show();
      void hide();
      int visible();
      Fl_Window *win_main;
      Fl_Box *lbl_inte;
      Fl_Input *txt_cogn;
      Fl_Input *txt_nome;
      Fl_Choice *cbo_sess;
      Fl_Input *txt_data;
      Fl_Box *lbl_data;
      Fl_Output *lbl_luna;
      Fl_Output *lbl_outc;
      Fl_Button *btn_selc;
      Fl_Button *btn_calc;
      Fl_Button *btn_newc;
      Fl_Button *btn_esci;
      Fl_Button *btn_info;

   private:
      static Fl_Menu_Item sex[];
      int gg;
      int mm;
      int aa;
      string codcat;

      inline void
      cb_win_main_i();

      static void
      cb_win_main(Fl_Window*, void*);

      inline void
      cb_txt_cogn_i();

      static void
      cb_txt_cogn(Fl_Input*, void*);

      inline void
      cb_txt_nome_i();

      static void
      cb_txt_nome(Fl_Input*, void*);

      inline void
      cb_cbo_sess_i();

      static void
      cb_cbo_sess(Fl_Input*, void*);

      inline void
      cb_txt_data_i();

      static void
      cb_txt_data(Fl_Input*, void*);

      inline void
      cb_lbl_luna_i();

      static void
      cb_lbl_luna(Fl_Output*, void*);

      static void
      cb_lbl_outc(Fl_Output*, void*);

      inline void
      cb_btn_selc_i();

      static void
      cb_btn_selc(Fl_Button*, void*);

      inline void
      cb_btn_calc_i();

      static void
      cb_btn_calc(Fl_Button*, void*);

      inline void
      cb_btn_newc_i();

      static void
      cb_btn_newc(Fl_Button*, void*);

      static void
      cb_btn_esci(Fl_Button*, void*);

      static void
      cb_btn_info(Fl_Button*, void*);

      inline void
      show_info();

      int
      check_data();

      void
      calcodfis();

      string
      trovacons(const string& input, const bool isnome);
};

#endif
