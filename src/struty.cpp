/**
   @license GNU GPLv2
   PROJECT "struty"
   Copyright � 2005,2006 Danilo Cicerone

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

/**
   @file struty.cpp
   @author Danilo Cicerone info@digitazero.org
   @date 2006-05-11
   @version 0.4.7
*/

#include "struty.h"

void
StrUty::split(const string& input, const string& delimiter, vector<string>& results)
{

   const string::size_type len = input.length();
   string::size_type i = 0;

   while (i < len)
   {
      i = input.find_first_not_of(delimiter, i);

      if (i == string::npos)
      return;

      string::size_type j = input.find_first_of(delimiter, i);

      if (j == string::npos)
      {
         results.push_back(input.substr(i));
         return;
      }
      else results.push_back(input.substr(i, j - i));

      i = j + 1;
   }
}

/*----------------------------------------------------------------------------*/

string
StrUty::trim(const string& input)
{
   string app;
   int strend = input.size() - 1;
   int strsta = 0;

   while (input[strsta] == ' ' && strsta < strend) strsta++;
   while (input[strend] == ' ' && strend >= 0) strend--;

   if (strend >= strsta) return app = input.substr(strsta, strend - strsta + 1);

   return "";
}

/*----------------------------------------------------------------------------*/

int
StrUty::toint(const string& input)
{
	istringstream app(input);

	int value;

	app >> value;

	return value;
}

/*----------------------------------------------------------------------------*/

double
StrUty::todouble(const string& input)
{
	istringstream app(input);

	double value;

	app >> value;

	return value;
}

/*----------------------------------------------------------------------------*/

string
StrUty::int2string(const int& input)
{
	ostringstream app;

	app << input;

	return app.str();
}

/*----------------------------------------------------------------------------*/

string
StrUty::lcase(const string& input)
{
   string app(input);

   for (unsigned int i = 0; i < app.size(); i++)
      app[i] = (char) tolower(app[i]);

   return app;
}

/*----------------------------------------------------------------------------*/

string
StrUty::ucase(const string& input)
{
   string app(input);

   for (unsigned int i = 0; i < app.size(); i++)
      app[i] = (char) toupper(app[i]);

   return app;
}
