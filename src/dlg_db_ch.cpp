/**
   @license GNU GPLv2
   PROJECT "codfis"
   Copyright � 2005,2006 Danilo Cicerone

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

/**
   @file dlg_db_ch.cpp
   @author Danilo Cicerone info@digitazero.org
   @date 2006-05-11
   @version 0.4.7
*/

#include "dlg_db_ch.h"

WinSelDB::WinSelDB()
{
   dlg_sele = new Fl_Window((Fl::w() - 400)/2, (Fl::h() - 355)/2, 400, 355, 
      "Selezione del Comune di Nascita");
   dlg_sele->user_data((void*)(this));
   dlg_sele->begin();
      fra_sel = new Fl_Box(5, 5, 390, 310);
      fra_sel->box(FL_ENGRAVED_FRAME);

      lst_db = new Fl_Hold_Browser(10, 10, 380, 240);
      lst_db->callback((Fl_Callback*)cb_lst_db, (void*)(this));

      txt_sch1 = new Fl_Input(70, 255, 320, 25, "Comune");
      txt_sch1->callback((Fl_Callback*)cb_txt_sch1, (void*)(this));
      txt_sch1->when(FL_WHEN_CHANGED);
      txt_sch1->take_focus();

      txt_sch2 = new Fl_Input(70, 285, 50, 25, "Provincia");
      txt_sch2->callback((Fl_Callback*)cb_txt_sch2, (void*)(this));
      txt_sch2->maximum_size(2);
      txt_sch2->when(FL_WHEN_CHANGED);

      btn_ok = new Fl_Return_Button(55, 325, 120, 25, "Ok");
      btn_ok->callback((Fl_Callback*)cb_btn_ok, (void*)(this));

      btn_annulla = new Fl_Button(225, 325, 120, 25, "Annulla");
      btn_annulla->callback((Fl_Callback*)cb_btn_annulla, (void*)(this));
   dlg_sele->end();
   dlg_sele->set_modal();

   int rc;
   rc = sqlite3_open("./codfis.db3", &db);
   if (rc)
   {
      fl_message("Database Error: %s", sqlite3_errmsg(db));
      sqlite3_close(db);
      exit(0);
   }
}

/*----------------------------------------------------------------------------*/

WinSelDB::~WinSelDB()
{
   delete dlg_sele;
}

/*----------------------------------------------------------------------------*/

void
WinSelDB::show()
{
   dlg_sele->show();
}

/*----------------------------------------------------------------------------*/

void
WinSelDB::hide()
{
   dlg_sele->hide();
   close_db();
}

/*----------------------------------------------------------------------------*/

int
WinSelDB::visible()
{
   return dlg_sele->visible();
}

/*----------------------------------------------------------------------------*/

void
WinSelDB::close_db()
{
   sqlite3_close(db);
}

/*----------------------------------------------------------------------------*/

int
WinSelDB::standby(string &strcod, string &strcom)
{
   int rc;
   string val;
   string tmp;
   string sql;
   sqlite3_stmt *pStmt;

   sql = "SELECT * FROM CodFis ORDER BY desc_cod;";
   lst_db->clear();
   do
   {
      rc = sqlite3_prepare(db, sql.c_str() , -1, &pStmt, 0);
      if (rc != SQLITE_OK)
      {
         fl_alert("Error in sqlite3_prepare: %s\n", sqlite3_errmsg(db));
         break;
      }

      while (SQLITE_ROW == sqlite3_step(pStmt))
      {
         string *ccad = new string("");

         if (sqlite3_column_type(pStmt, 2) == 5) val = "";
         else val = (char*)sqlite3_column_text(pStmt, 2);

         if (sqlite3_column_type(pStmt, 3) == 5) tmp = "";
         else tmp = (char*)sqlite3_column_text(pStmt, 3);

         if (sqlite3_column_type(pStmt, 4) != 5)
            ccad->assign((char*) sqlite3_column_text(pStmt, 4));

         val = val + " (" + tmp + ")";

         lst_db->add(val.c_str(), (void *)(ccad->c_str()));
      }
      rc = sqlite3_finalize(pStmt);

   } while(rc == SQLITE_SCHEMA);

   lst_db->select(1);
   show();
   while (visible()) Fl::wait();

   strcom = (char*)(lst_db->text(lst_db->value()));
   strcod = (char*)(lst_db->data(lst_db->value()));
   //fl_message("OK : %s", strcod.c_str());
   return clicked;
}

/*----------------------------------------------------------------------------*/

void
WinSelDB::cb_lst_db(Fl_Hold_Browser* o, void* obj)
{

}

/*----------------------------------------------------------------------------*/

void
WinSelDB::cb_txt_sch1(Fl_Input* o, void* obj)
{
   static_cast<WinSelDB *>(obj)->ex_query();
}

/*----------------------------------------------------------------------------*/

void
WinSelDB::cb_txt_sch2(Fl_Input* o, void* obj)
{
   static_cast<WinSelDB *>(obj)->ex_query();
}

/*----------------------------------------------------------------------------*/

void
WinSelDB::cb_btn_annulla(Fl_Button*, void* obj)
{
   static_cast<WinSelDB *>(obj)->ex_annulla();
}

/*----------------------------------------------------------------------------*/

inline void
WinSelDB::ex_annulla()
{
   clicked = 0;
   hide();
}

/*----------------------------------------------------------------------------*/

void
WinSelDB::cb_btn_ok(Fl_Return_Button*, void* obj)
{
   static_cast<WinSelDB *>(obj)->ex_ok();
}

/*----------------------------------------------------------------------------*/

inline void
WinSelDB::ex_ok()
{
   clicked = 1;
   hide();
}

/*----------------------------------------------------------------------------*/

inline void
WinSelDB::ex_query()
{
   int rc;
   string val;
   string tmp;
   string sql;
   sqlite3_stmt *pStmt;

   lst_db->clear();
   sql = sqlite3_mprintf("SELECT * FROM CodFis WHERE desc_cod LIKE '%q%%'"
      " AND prov_cod LIKE '%q%%' ORDER BY desc_cod;",
      txt_sch1->value(), txt_sch2->value());
   //fl_message("[%s]", ssql.c_str());

   lst_db->clear();
   do
   {
      rc = sqlite3_prepare(db, sql.c_str() , -1, &pStmt, 0);
      if (rc != SQLITE_OK)
      {
         fl_alert("Error in sqlite3_prepare: %s\n", sqlite3_errmsg(db));
         break;
      }

      while (SQLITE_ROW == sqlite3_step(pStmt))
      {
         string *ccad = new string("");

         if (sqlite3_column_type(pStmt, 2) == 5) val = "";
         else val = (char*)sqlite3_column_text(pStmt, 2);

         if (sqlite3_column_type(pStmt, 3) == 5) tmp = "";
         else tmp = (char*)sqlite3_column_text(pStmt, 3);

         if (sqlite3_column_type(pStmt, 4) != 5)
            ccad->assign((char*) sqlite3_column_text(pStmt, 4));

         val = val + " (" + tmp + ")";

         lst_db->add(val.c_str(), (void *)(ccad->c_str()));
      }
      rc = sqlite3_finalize(pStmt);

   } while(rc == SQLITE_SCHEMA);

   lst_db->select(1);
}
