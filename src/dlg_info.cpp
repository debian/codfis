/**
   @license GNU GPLv2
   PROJECT "codfis"
   Copyright � 2005,2006 Danilo Cicerone

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

/**
   @file dlg_db_ch.cpp
   @author Danilo Cicerone info@digitazero.org
   @date 2006-05-11
   @version 0.4.7
*/

#include "dlg_info.h"

/*----------------------------------------------------------------------------*/

WinInfo::WinInfo()
{
   string lbl;
   lbl = "codfis 0.4.7\n";
   lbl += "Copyright � 2005,2006 Danilo Cicerone\n";
   lbl += "This is free software; see the source for copying conditions. ";
   lbl += "There is NO warranty; not even for MERCHANTABILITY or ";
   lbl += "FITNESS FOR A PARTICULAR PURPOSE.\n\n";
   lbl += "Credits:\n";
   lbl += "- Giuseppe Borz� (for providing Ubuntu packages);\n\n";
   lbl += "Special Thanks:\n";
   lbl += "- Francesco Poli (for constructive criticism);\n\n";
   lbl += "Project Page: http://www.digitazero.org/?p=16\n";
   lbl += "Report bugs to: info@@digitazero.org\n";
   dlg_info = new Fl_Window((Fl::w() - 400)/2, (Fl::h() - 320)/2, 400, 320);
   dlg_info->user_data((void*)(this));
   dlg_info->begin();
   dlg_info->box(FL_THIN_UP_BOX);
   dlg_info->color(FL_BACKGROUND2_COLOR);
   
      lbl_info = new Fl_Box(3, 3, 392, 265);
      //lbl_info->box(FL_THIN_UP_BOX);
      lbl_info->align(165|FL_ALIGN_INSIDE);
      lbl_info->color(FL_BACKGROUND2_COLOR);
      lbl_info->labelfont(1);
      lbl_info->labelsize(14);
      lbl_info->copy_label(lbl.c_str());

      btn_annulla = new Fl_Button(150, 290, 100, 25, "&Chiudi");
      btn_annulla->callback((Fl_Callback*)cb_btn_annulla, (void*)(this));
      btn_annulla->labelcolor(FL_BACKGROUND2_COLOR);
      btn_annulla->color((Fl_Color)59);

   dlg_info->clear_border();
   dlg_info->end();
   dlg_info->set_modal();
}

/*----------------------------------------------------------------------------*/

WinInfo::~WinInfo()
{
   delete dlg_info;
}

/*----------------------------------------------------------------------------*/

void
WinInfo::show()
{
   dlg_info->show();
}

/*----------------------------------------------------------------------------*/

void
WinInfo::hide()
{
   dlg_info->hide();
}

/*----------------------------------------------------------------------------*/

int
WinInfo::visible()
{
   return dlg_info->visible();
}

/*----------------------------------------------------------------------------*/

int
WinInfo::standby()
{
   show();
   while (visible()) Fl::wait();
   
   return 0;
}

/*----------------------------------------------------------------------------*/

void
WinInfo::cb_btn_annulla(Fl_Button*, void* obj)
{
   static_cast<WinInfo *>(obj)->ex_annulla();
}

/*----------------------------------------------------------------------------*/

inline void
WinInfo::ex_annulla()
{
   hide();
}
