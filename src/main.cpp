/**
   @license GNU GPLv2
   PROJECT "codfis"
   Copyright � 2005,2006 Danilo Cicerone

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

/**
   @file main.cpp
   @author Danilo Cicerone info@digitazero.org
   @date 2006-05-11
   @version 0.4.7
*/

#include "calccf.h"

int
main (int argc, char *argv[])
{
   CFWin *win = new CFWin();

   win->show();

   return Fl::run();
}
